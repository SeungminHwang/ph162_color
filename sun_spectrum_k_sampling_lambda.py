#sun_spectrum_k_sampling
import numpy as np
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt


fig, ax = plt.subplots(figsize = (5, 5))

k = 7
k -= 1
lam = np.arange(370, 680, (679-370)/k)
#freq = np.arange(429, 790, (789-429)/k)

I = (1.1916e29)/(lam**5)/(np.exp(2.493e3/lam)-1)


Sum = np.sum(I)
p = I/Sum
Str = ""

f = open("K-Sampling.txt", "w")
for x in p:
	Str += str(x) + "\t"
f.write(Str)
f.write("\n")
Str = ""
for x in lam:
	Str += str(x) + "\t"
f.write(Str)
f.close()

ax.text(400, 2.1e13, "T = 5772K, Sun", fontsize=11)
ax.text(500, 0.5e13, "T = 4000K", fontsize=11)
ax.plot(lam, I, "o")

lam = np.arange(370, 680, 1)
#lam = np.arange(50, 2500, 1)
I = (1.1916e29)/(lam**5)/(np.exp(2.493e3/lam)-1)
#I2 = (1.1916e29)/(lam**5)/(np.exp(2.493e3*5772/4000/lam)-1)
ax.plot(lam, I)
#ax.plot(lam, I2)

ax.set_title("Blackbody Radiation K-Sampling")
ax.set_xlabel("Wavelength [nm]")
plt.show()