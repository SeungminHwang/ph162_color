import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np


def get_absorption_data():
	# get absorption data from text file
	f = open("absorb_data.txt", "r")	

	B = []
	num_b = int(f.readline())
	b_data = f.readline()
	b_lambda = f.readline()
	b_data = b_data.split("\t")
	b_lambda = b_lambda.split("\t")
	for i in range(num_b):
		b_data[i] = float(b_data[i])
		b_lambda[i] = int(b_lambda[i])
		B.append((b_lambda[i],b_data[i]))	
	
	G = []
	num_g = int(f.readline())
	g_data = f.readline()
	g_lambda = f.readline()
	g_data = g_data.split("\t")
	g_lambda = g_lambda.split("\t")
	for i in range(num_g):
		g_data[i] = float(g_data[i])
		g_lambda[i] = int(g_lambda[i])
		G.append((g_lambda[i],g_data[i]))	

	R = []
	num_r = int(f.readline())
	r_data = f.readline()
	r_lambda = f.readline()
	r_data = r_data.split("\t")
	r_lambda = r_lambda.split("\t")
	for i in range(num_r):
		r_data[i] = float(r_data[i])
		r_lambda[i] = int(r_lambda[i])
		R.append((r_lambda[i],r_data[i]))

	f.close()

	return (R, G, B)


def get_estimated_absorption(x, data):
	weight = [1, 1, 1]

	absorb = [0, 0, 0]
	idx0 = [400, 400, 370]
	for i in range(3):
		a = data[i][0][0]
		b = data[i][-1][0]
		if(x>=b or x<a):
			absorb[i] = 0
			continue
		a = x - (x%10)
		na = (a - idx0[i])//10
		nb = na + 1
		b = a + 10


		result = data[i][na][1]*(b - x) + data[i][nb][1]*(x - a)
		result /= 10

		absorb[i] = result*weight[i]

	return absorb

def graph_plot(data):
	color_table = ["r", "g", "b"]
	k = 0
	fig, ax = plt.subplots(figsize = (8, 4))
	for color in data:
		x = []
		y = []
		for i in range(len(color)):
			x.append(color[i][0])
			y.append(color[i][1])

		ax.plot(x, y, color = color_table[k])
		k+=1
	
	ax.set_title("Normalized Absorbance")
	plt.show()


def get_squared_error(data):

	N = len(data)
	Sum = 0
	for x in data:
		Sum += x
	mean = Sum/N

	result = 0
	for x in data:
		result += (mean - x)*(mean - x)


	#result /= N
	return result

def get_mean_abs_error(data):
	N = len(data)
	Sum = 0
	for x in data:
		Sum += x
	mean = Sum/N

	result = 0
	for x in data:
		result += abs(mean - x)


	#result /= N
	return result


def find_complement(color1, data, mode = 2):
	min_val = 987654321
	result = []
	r1, g1, b1 = get_estimated_absorption(color1, data)
	for i in range(370, 680):
		r2, g2, b2 = get_estimated_absorption(i, data)

		r = r1 + r2
		b = b1 + b2
		g = g1 + g2

		if(mode == 1):
			val = get_squared_error([r, g, b])
		if(mode == 2):
			val = get_mean_abs_error([r, g, b])
		if(val<min_val):
			min_val = val
			result = [i, r, g, b, min_val]

	return result







data = get_absorption_data()
#print(data)


print(get_estimated_absorption(530, data))
print(get_estimated_absorption(580, data))

print(find_complement(550, data))





fig, ax = plt.subplots(figsize = (5, 5))
t = np.arange(370, 681, 1)
t1 = np.arange(525, 681, 1)
t2 = np.arange(370, 525, 1)
ax.plot(t1, t1-155)
ax.plot(t2, t2+155)

#x = [525]*311
#ax.plot(x, t)

f = open("output.txt", "w")
error_max = -1
set_x = []
set_y = []
color = []

for i in range(370, 680):
	y = find_complement(i, data, 1)
	set_y.append(y[0])
	set_x.append(i)
	color.append(y[4])
	#f.write(str(i) + "\t" + str(y[0]) + "\t" + str(y[4]) + "\n")
	if(error_max<y[4]):
		error_max = y[4]
	

f.close()

for i in range(len(set_x)):
	ax.plot(set_x[i], set_y[i], "o", color = (color[i]/error_max, ((error_max - color[i])/error_max), 0))


#plot
#ax.plot(set_x,set_y, "o")
#ax.plot(set_y,set_x, "o")


ax.set_xlabel("color [nm]")
ax.set_ylabel("complement [nm]")
plt.show()



#graph_plot(data)










