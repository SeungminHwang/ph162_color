#sun_spectrum_k_sampling
import numpy as np
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt


fig, ax = plt.subplots(figsize = (6, 6))

k = int(input())
f = open("color1.txt", "r")
p = f.readline()
p = p.split("\t")

lam = f.readline()
lam = lam.split("\t")
f.close()

print(lam, len(p))


x = []
y = []
for i in range(k):
	x.append(float(lam[i]))
	y.append(float(p[i]))
ax.plot(x, y, "o", color = "r")


f = open("k_sample.txt", "r")
p = f.readline()
p = p.split("\t")
lam = f.readline()
lam = lam.split("\t")
f.close()

x = []
y = []
for i in range(k):
	x.append(float(lam[i]))
	y.append(float(p[i]))
ax.plot(x, y, "o", color = "b")







#ax.text(565, 7.5, "T = 5772K, Sun", fontsize=11)
#ax.text(250, 1.5, "T = 3000K", fontsize=11)

lam = np.arange(370, 680, 1)
I = (1.1916e29)/(lam**5)/(np.exp(2.493e3/lam)-1)
#ax.plot(lam, I)

ax.set_title("Color1 Complement result")
ax.set_xlabel("Wavelength [nm]")
ax.set_ylabel("proportion")
plt.show()