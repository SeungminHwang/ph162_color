#linear_regression

def y_predict_1(a, b, x):
	return a*x + b

def y_predict_2(a, b, x):
	return x/a - b/a



x1 = []
y1 = []

x2 = []
y2 = []


f = open("reg.txt", "r")
for i in range(55):
	data = f.readline()
	data = data.split("\t")

	x = int(data[0])
	y = int(data[1])
	x1.append(x)
	y1.append(y)


for i in range(125):
	data = f.readline()
	data = data.split("\t")

	x = int(data[0])
	y = int(data[1])
	x2.append(x)
	y2.append(y)



minMSE = 987654321

for i in range(100):
	for j in range(100):
		a = 1.00 + 0.01*i
		b = 100 + j

		MSE = 0

		for k in range(55):
			MSE += (y_predict_1(a, b, x1[k]) - y1[k])**2
		for k in range(125):
			MSE += (y_predict_2(a, b, x2[k]) - y2[k])**2


		if(minMSE > MSE):
			minMSE = MSE
			result = (a, b, minMSE)


print(result)
print(1/result[0], -result[1]/result[0], result[2])

