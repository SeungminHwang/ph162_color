#sun_spectrum_k_sampling
import numpy as np
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt


fig, ax = plt.subplots(figsize = (4, 4))

k = 10
k -= 1
freq = np.arange(429, 790, (789-429)/k)
I = freq*freq*freq*(4.41e-6)/(np.exp(8.31e-3*freq)-1)
#I2 = freq*freq*freq*(4.41e-6)/(np.exp(8.31e-3/3000*5772*freq)-1)

ax.text(565, 7.5, "T = 5772K, Sun", fontsize=11)
#ax.text(250, 1.5, "T = 3000K", fontsize=11)
ax.plot(freq, I, "o")

freq = np.arange(429, 789, 1)
I = freq*freq*freq*(4.41e-6)/(np.exp(8.31e-3*freq)-1)
ax.plot(freq, I)
#ax.plot(freq, I2)
ax.set_title("Blackbody Radiation K-Sampling")
ax.set_xlabel("Freq [1e-12 Hz]")
plt.show()