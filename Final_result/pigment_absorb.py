import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np

def get_absorption_data():
	# get absorption data from text file
	f = open("pigment.txt", "r")	

	R = []
	num_r = int(f.readline())
	r_lambda = f.readline()
	r_data = f.readline()
	r_data = r_data.split("\t")
	r_lambda = r_lambda.split("\t")
	for i in range(num_r):
		r_data[i] = float(r_data[i])
		r_lambda[i] = int(r_lambda[i])
		R.append((r_lambda[i],r_data[i]))
		
	
	G = []
	num_g = int(f.readline())
	g_lambda = f.readline()
	g_data = f.readline()
	g_data = g_data.split("\t")
	g_lambda = g_lambda.split("\t")
	for i in range(num_g):
		g_data[i] = float(g_data[i])
		g_lambda[i] = int(g_lambda[i])
		G.append((g_lambda[i],g_data[i]))	

	
	B = []
	num_b = int(f.readline())
	b_lambda = f.readline()
	b_data = f.readline()
	b_data = b_data.split("\t")
	b_lambda = b_lambda.split("\t")
	for i in range(num_b):
		b_data[i] = float(b_data[i])
		b_lambda[i] = int(b_lambda[i])
		B.append((b_lambda[i],b_data[i]))

	f.close()

	return (R, G, B)

def get_estimated_absorption(x, data):
	weight = [1, 1, 1]

	absorb = [0, 0, 0]
	idx0 = [380, 380, 380]
	for i in range(3):
		a = data[i][0][0]
		b = data[i][-1][0]
		if(x>=b or x<a):
			absorb[i] = 0
			continue
		a = x - (x%10)
		na = (a - idx0[i])//10
		nb = na + 1
		b = a + 10


		result = data[i][na][1]*(b - x) + data[i][nb][1]*(x - a)
		result /= 10

		absorb[i] = result*weight[i]

	return absorb

def absorbance_graph_plot(data):
	color_table = ["r", "b", "g"]
	k = 0
	fig, ax = plt.subplots(figsize = (8, 4))
	for color in data:
		x = []
		y = []
		for i in range(len(color)):
			x.append(color[i][0])
			y.append(color[i][1])

		ax.plot(x, y, color = color_table[k])
		k+=1
	
	ax.set_title("Photosynthesis Pigment Absorbance")
	ax.text(365, 0.5, "Chlorophyll a", fontsize=11)
	ax.text(465, 0.8, "Chlorophyll b", fontsize=11)
	ax.text(505, 0.6, "Carotenoids", fontsize=11)
	ax.set_xlabel("Wavelength [nm]")
	ax.set_ylabel("Absorbance")
	plt.show()


data = get_absorption_data()
absorbance_graph_plot(data)


print(get_estimated_absorption(405, data))
