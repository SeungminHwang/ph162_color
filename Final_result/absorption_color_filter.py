def get_absorption_data():
	# get absorption data from text file
	f = open("pigment.txt", "r")	

	R = []
	num_r = int(f.readline())
	r_lambda = f.readline()
	r_data = f.readline()
	r_data = r_data.split("\t")
	r_lambda = r_lambda.split("\t")
	for i in range(num_r):
		r_data[i] = float(r_data[i])
		r_lambda[i] = int(r_lambda[i])
		R.append((r_lambda[i],r_data[i]))
		
	
	G = []
	num_g = int(f.readline())
	g_lambda = f.readline()
	g_data = f.readline()
	g_data = g_data.split("\t")
	g_lambda = g_lambda.split("\t")
	for i in range(num_g):
		g_data[i] = float(g_data[i])
		g_lambda[i] = int(g_lambda[i])
		G.append((g_lambda[i],g_data[i]))	

	
	B = []
	num_b = int(f.readline())
	b_lambda = f.readline()
	b_data = f.readline()
	b_data = b_data.split("\t")
	b_lambda = b_lambda.split("\t")
	for i in range(num_b):
		b_data[i] = float(b_data[i])
		b_lambda[i] = int(b_lambda[i])
		B.append((b_lambda[i],b_data[i]))

	f.close()

	return (R, G, B)

def get_estimated_absorption(x, data):
	weight = [1, 1, 1]

	absorb = [0, 0, 0]
	idx0 = [380, 380, 380]
	for i in range(3):
		a = data[i][0][0]
		b = data[i][-1][0]
		if(x>=b or x<a):
			absorb[i] = 0
			continue
		a = x - (x%10)
		na = (a - idx0[i])//10
		nb = na + 1
		b = a + 10


		result = data[i][na][1]*(b - x) + data[i][nb][1]*(x - a)
		result /= 10

		absorb[i] = result*weight[i]

	return absorb


def get_reflected_light(it):
	global p
	absorbance = get_estimated_absorption(int(lam[it]), data)
	
	absorbance = absorbance[0] + absorbance[1] + absorbance[2]
	p[it] -= absorbance*p[it]

	#boundary Exception handling
	if(p[it]>1):
		p[it] = 1
	if(p[it]<0):
		p[it] = 0


def complement(x):
	if(x<480):
		return 1.14*x + 100
	else:
		return 0.877*x - 87.72


def get_complement_light(it):
	global p, lam, new_p, new_lam
	absorbance = get_estimated_absorption(int(lam[it]), data)
	absorbance = absorbance[0] + absorbance[1] + absorbance[2]

	np = p[it]
	p[it] -= absorbance*p[it]

	np = absorbance*np
	nlam = complement(lam[i])

	if(p[it]>1):
		p[it] = 1
	if(p[it]<0):
		p[it] = 0
	if(np>0.03):
		np = 0.03
	if(np < 0):
		np = 0
	new_p.append(np)
	new_lam.append(nlam)



new_p = []
new_lam = []

#program start line
data = get_absorption_data()


k = int(input())
f = open("k_sample.txt", "r")
p = f.readline()
p = p.split("\t")
lam = f.readline()
lam = lam.split("\t")
p[-1] = 0
lam[-1] = 0
f.close()

#print(p)


for i in range(k):
	p[i] = float(p[i])
	lam[i] = float(lam[i])
	#get_reflected_light(i)
	get_complement_light(i)

print(p)

f = open("color1.txt", "w")
for i in range(k):
	f.write(str(new_p[i]) + "\t")

f.write("\n")
for i in range(k):
	f.write(str(new_lam[i]) + "\t")
f.close()


