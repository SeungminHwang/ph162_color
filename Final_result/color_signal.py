import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np


def get_absorption_data():
	# get absorption data from text file
	f = open("absorb_data.txt", "r")	

	B = []
	num_b = int(f.readline())
	b_data = f.readline()
	b_lambda = f.readline()
	b_data = b_data.split("\t")
	b_lambda = b_lambda.split("\t")
	for i in range(num_b):
		b_data[i] = float(b_data[i])
		b_lambda[i] = int(b_lambda[i])
		B.append((b_lambda[i],b_data[i]))	
	
	G = []
	num_g = int(f.readline())
	g_data = f.readline()
	g_lambda = f.readline()
	g_data = g_data.split("\t")
	g_lambda = g_lambda.split("\t")
	for i in range(num_g):
		g_data[i] = float(g_data[i])
		g_lambda[i] = int(g_lambda[i])
		G.append((g_lambda[i],g_data[i]))	

	R = []
	num_r = int(f.readline())
	r_data = f.readline()
	r_lambda = f.readline()
	r_data = r_data.split("\t")
	r_lambda = r_lambda.split("\t")
	for i in range(num_r):
		r_data[i] = float(r_data[i])
		r_lambda[i] = int(r_lambda[i])
		R.append((r_lambda[i],r_data[i]))

	f.close()

	return (R, G, B)


def get_estimated_absorption(x, data):
	weight = [1, 1, 1]

	absorb = [0, 0, 0]
	idx0 = [400, 400, 370]
	for i in range(3):
		a = data[i][0][0]
		b = data[i][-1][0]
		if(x>=b or x<a):
			absorb[i] = 0
			continue
		a = x - (x%10)
		na = (a - idx0[i])//10
		nb = na + 1
		b = a + 10


		result = data[i][na][1]*(b - x) + data[i][nb][1]*(x - a)
		result /= 10

		absorb[i] = result*weight[i]

	return absorb



#program start
data = get_absorption_data()
k = int(input())
f = open("k_sample.txt", "r")
p = f.readline()
p = p.split("\t")
lam = f.readline()
lam = lam.split("\t")
f.close()

R = 0
G = 0
B = 0
p_sum = 0
for i in range(k):
	p_sum += float(p[i])
	result = get_estimated_absorption(int(float(lam[i])), data)
	R += result[0]*float(p[i])
	G += result[1]*float(p[i])
	B += result[2]*float(p[i])
rgbSum = R + G + B
#print(R, G, B)
print(R/rgbSum, G/rgbSum, B/rgbSum)






